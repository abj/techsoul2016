<?php

/* utilisateurBundle:admin:projetButAtteint.html.twig */
class __TwigTemplate_f081b06670b32955e603ea385289d91200f6494980c3dedb49bee6ebce304a7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin/navbar.html.twig", "utilisateurBundle:admin:projetButAtteint.html.twig", 1);
        $this->blocks = array(
            'task' => array($this, 'block_task'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin/navbar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_task($context, array $blocks = array())
    {
        // line 3
        echo "<tr>
\t\t\t\t\t\t\t\t<td><span class=\"status status-success item-before\"></span> <a href=\"#\">Frontpage fixes</a></td>
\t\t\t\t\t\t\t\t<td><span class=\"text-smaller text-semibold\">Bugs</span></td>
\t\t\t\t\t\t\t\t<td class=\"text-center\"><span class=\"label label-success\">87%</span></td>
\t\t\t\t\t\t\t</tr>
                                                        ";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:admin:projetButAtteint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "::admin/navbar.html.twig" %}*/
/* {% block task %}*/
/* <tr>*/
/* 								<td><span class="status status-success item-before"></span> <a href="#">Frontpage fixes</a></td>*/
/* 								<td><span class="text-smaller text-semibold">Bugs</span></td>*/
/* 								<td class="text-center"><span class="label label-success">87%</span></td>*/
/* 							</tr>*/
/*                                                         {% endblock %}*/
/*                                                         */
/*                                                         */
/*                                                         */
