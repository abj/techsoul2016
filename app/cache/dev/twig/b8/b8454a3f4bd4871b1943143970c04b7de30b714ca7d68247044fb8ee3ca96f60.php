<?php

/* crowdBundle:Default/Evenement:Calendrier_evenements.html.twig */
class __TwigTemplate_95cea67bf7a5db011a01cae5edac03bc445b76ef2157ee1b181293197b37cb5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "crowdBundle:Default/Evenement:Calendrier_evenements.html.twig", 2);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_css($context, array $blocks = array())
    {
        // line 4
        echo "    
    
        <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/calendrier/calendrier.css"), "html", null, true);
        echo "\">


";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "

    <!-- Page Title -->
    <div class=\"section section-breadcrumbs\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <h1>liste des evenements</h1>
                </div>
            </div>
        </div>
    </div>

<br/>
<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"[ col-xs-12 col-sm-offset-2 col-sm-8 ]\">
\t\t\t\t<ul class=\"event-list\">
\t\t\t\t\t<li>
\t\t\t\t\t\t<time datetime=\"2014-07-20\">
\t\t\t\t\t\t\t<span class=\"day\">4</span>
\t\t\t\t\t\t\t<span class=\"month\">Jul</span>
\t\t\t\t\t\t\t<span class=\"year\">2014</span>
\t\t\t\t\t\t\t<span class=\"time\">ALL DAY</span>
\t\t\t\t\t\t</time>
\t\t\t\t\t\t<img alt=\"Independence Day\" src=\"https://farm4.staticflickr.com/3100/2693171833_3545fb852c_q.jpg\" />
\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t<h2 class=\"title\">Independence Day</h2>
\t\t\t\t\t\t\t<p class=\"desc\">United States Holiday</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"social\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li class=\"facebook\" style=\"width:33%;\"><a href=\"#facebook\"><span class=\"fa fa-facebook\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"twitter\" style=\"width:34%;\"><a href=\"#twitter\"><span class=\"fa fa-twitter\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"google-plus\" style=\"width:33%;\"><a href=\"#google-plus\"><span class=\"fa fa-google-plus\"></span></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t\t\t<li>
\t\t\t\t\t\t<time datetime=\"2014-07-20 0000\">
\t\t\t\t\t\t\t<span class=\"day\">8</span>
\t\t\t\t\t\t\t<span class=\"month\">Jul</span>
\t\t\t\t\t\t\t<span class=\"year\">2014</span>
\t\t\t\t\t\t\t<span class=\"time\">12:00 AM</span>
\t\t\t\t\t\t</time>
\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t<h2 class=\"title\">One Piece Unlimited World Red</h2>
\t\t\t\t\t\t\t<p class=\"desc\">PS Vita</p>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li style=\"width:50%;\"><a href=\"#website\"><span class=\"fa fa-globe\"></span> Website</a></li>
\t\t\t\t\t\t\t\t<li style=\"width:50%;\"><span class=\"fa fa-money\"></span> \$39.99</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"social\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li class=\"facebook\" style=\"width:33%;\"><a href=\"#facebook\"><span class=\"fa fa-facebook\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"twitter\" style=\"width:34%;\"><a href=\"#twitter\"><span class=\"fa fa-twitter\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"google-plus\" style=\"width:33%;\"><a href=\"#google-plus\"><span class=\"fa fa-google-plus\"></span></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t\t\t<li>
\t\t\t\t\t\t<time datetime=\"2014-07-20 2000\">
\t\t\t\t\t\t\t<span class=\"day\">20</span>
\t\t\t\t\t\t\t<span class=\"month\">Jan</span>
\t\t\t\t\t\t\t<span class=\"year\">2014</span>
\t\t\t\t\t\t\t<span class=\"time\">8:00 PM</span>
\t\t\t\t\t\t</time>
\t\t\t\t\t\t<img alt=\"My 24th Birthday!\" src=\"https://farm5.staticflickr.com/4150/5045502202_1d867c8a41_q.jpg\" />
\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t<h2 class=\"title\">Mouse0270's 24th Birthday!</h2>
\t\t\t\t\t\t\t<p class=\"desc\">Bar Hopping in Erie, Pa.</p>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li style=\"width:33%;\">1 <span class=\"glyphicon glyphicon-ok\"></span></li>
\t\t\t\t\t\t\t\t<li style=\"width:34%;\">3 <span class=\"fa fa-question\"></span></li>
\t\t\t\t\t\t\t\t<li style=\"width:33%;\">103 <span class=\"fa fa-envelope\"></span></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"social\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li class=\"facebook\" style=\"width:33%;\"><a href=\"#facebook\"><span class=\"fa fa-facebook\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"twitter\" style=\"width:34%;\"><a href=\"#twitter\"><span class=\"fa fa-twitter\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"google-plus\" style=\"width:33%;\"><a href=\"#google-plus\"><span class=\"fa fa-google-plus\"></span></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t\t\t<li>
\t\t\t\t\t\t<time datetime=\"2014-07-31 1600\">
\t\t\t\t\t\t\t<span class=\"day\">31</span>
\t\t\t\t\t\t\t<span class=\"month\">Jan</span>
\t\t\t\t\t\t\t<span class=\"year\">2014</span>
\t\t\t\t\t\t\t<span class=\"time\">4:00 PM</span>
\t\t\t\t\t\t</time>
\t\t\t\t\t\t<img alt=\"Disney Junior Live On Tour!\" src=\"http://www.thechaifetzarena.com/images/main/DL13_PiratePrincess_thumb.jpg\" />
\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t<h2 class=\"title\">Disney Junior Live On Tour!</h2>
\t\t\t\t\t\t\t<p class=\"desc\"> Pirate and Princess Adventure</p>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li style=\"width:33%;\">\$49.99 <span class=\"fa fa-male\"></span></li>
\t\t\t\t\t\t\t\t<li style=\"width:34%;\">\$29.99 <span class=\"fa fa-child\"></span></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"social\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li class=\"facebook\" style=\"width:33%;\"><a href=\"#facebook\"><span class=\"fa fa-facebook\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"twitter\" style=\"width:34%;\"><a href=\"#twitter\"><span class=\"fa fa-twitter\"></span></a></li>
\t\t\t\t\t\t\t\t<li class=\"google-plus\" style=\"width:33%;\"><a href=\"#google-plus\"><span class=\"fa fa-google-plus\"></span></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    // line 133
    public function block_javascripts($context, array $blocks = array())
    {
        // line 134
        echo "


";
    }

    public function getTemplateName()
    {
        return "crowdBundle:Default/Evenement:Calendrier_evenements.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 134,  169 => 133,  48 => 14,  45 => 13,  37 => 6,  33 => 4,  30 => 3,  11 => 2,);
    }
}
/* */
/* {% extends "::base.html.twig" %}*/
/* {% block css %}*/
/*     */
/*     */
/*         <link rel="stylesheet" href="{{asset('css/calendrier/calendrier.css')}}">*/
/* */
/* */
/* {% endblock%}*/
/* */
/* */
/* */
/* {% block body %}*/
/* */
/* */
/*     <!-- Page Title -->*/
/*     <div class="section section-breadcrumbs">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <h1>liste des evenements</h1>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* <br/>*/
/* <div class="container">*/
/* 		<div class="row">*/
/* 			<div class="[ col-xs-12 col-sm-offset-2 col-sm-8 ]">*/
/* 				<ul class="event-list">*/
/* 					<li>*/
/* 						<time datetime="2014-07-20">*/
/* 							<span class="day">4</span>*/
/* 							<span class="month">Jul</span>*/
/* 							<span class="year">2014</span>*/
/* 							<span class="time">ALL DAY</span>*/
/* 						</time>*/
/* 						<img alt="Independence Day" src="https://farm4.staticflickr.com/3100/2693171833_3545fb852c_q.jpg" />*/
/* 						<div class="info">*/
/* 							<h2 class="title">Independence Day</h2>*/
/* 							<p class="desc">United States Holiday</p>*/
/* 						</div>*/
/* 						<div class="social">*/
/* 							<ul>*/
/* 								<li class="facebook" style="width:33%;"><a href="#facebook"><span class="fa fa-facebook"></span></a></li>*/
/* 								<li class="twitter" style="width:34%;"><a href="#twitter"><span class="fa fa-twitter"></span></a></li>*/
/* 								<li class="google-plus" style="width:33%;"><a href="#google-plus"><span class="fa fa-google-plus"></span></a></li>*/
/* 							</ul>*/
/* 						</div>*/
/* 					</li>*/
/* */
/* 					<li>*/
/* 						<time datetime="2014-07-20 0000">*/
/* 							<span class="day">8</span>*/
/* 							<span class="month">Jul</span>*/
/* 							<span class="year">2014</span>*/
/* 							<span class="time">12:00 AM</span>*/
/* 						</time>*/
/* 						<div class="info">*/
/* 							<h2 class="title">One Piece Unlimited World Red</h2>*/
/* 							<p class="desc">PS Vita</p>*/
/* 							<ul>*/
/* 								<li style="width:50%;"><a href="#website"><span class="fa fa-globe"></span> Website</a></li>*/
/* 								<li style="width:50%;"><span class="fa fa-money"></span> $39.99</li>*/
/* 							</ul>*/
/* 						</div>*/
/* 						<div class="social">*/
/* 							<ul>*/
/* 								<li class="facebook" style="width:33%;"><a href="#facebook"><span class="fa fa-facebook"></span></a></li>*/
/* 								<li class="twitter" style="width:34%;"><a href="#twitter"><span class="fa fa-twitter"></span></a></li>*/
/* 								<li class="google-plus" style="width:33%;"><a href="#google-plus"><span class="fa fa-google-plus"></span></a></li>*/
/* 							</ul>*/
/* 						</div>*/
/* 					</li>*/
/* */
/* 					<li>*/
/* 						<time datetime="2014-07-20 2000">*/
/* 							<span class="day">20</span>*/
/* 							<span class="month">Jan</span>*/
/* 							<span class="year">2014</span>*/
/* 							<span class="time">8:00 PM</span>*/
/* 						</time>*/
/* 						<img alt="My 24th Birthday!" src="https://farm5.staticflickr.com/4150/5045502202_1d867c8a41_q.jpg" />*/
/* 						<div class="info">*/
/* 							<h2 class="title">Mouse0270's 24th Birthday!</h2>*/
/* 							<p class="desc">Bar Hopping in Erie, Pa.</p>*/
/* 							<ul>*/
/* 								<li style="width:33%;">1 <span class="glyphicon glyphicon-ok"></span></li>*/
/* 								<li style="width:34%;">3 <span class="fa fa-question"></span></li>*/
/* 								<li style="width:33%;">103 <span class="fa fa-envelope"></span></li>*/
/* 							</ul>*/
/* 						</div>*/
/* 						<div class="social">*/
/* 							<ul>*/
/* 								<li class="facebook" style="width:33%;"><a href="#facebook"><span class="fa fa-facebook"></span></a></li>*/
/* 								<li class="twitter" style="width:34%;"><a href="#twitter"><span class="fa fa-twitter"></span></a></li>*/
/* 								<li class="google-plus" style="width:33%;"><a href="#google-plus"><span class="fa fa-google-plus"></span></a></li>*/
/* 							</ul>*/
/* 						</div>*/
/* 					</li>*/
/* */
/* 					<li>*/
/* 						<time datetime="2014-07-31 1600">*/
/* 							<span class="day">31</span>*/
/* 							<span class="month">Jan</span>*/
/* 							<span class="year">2014</span>*/
/* 							<span class="time">4:00 PM</span>*/
/* 						</time>*/
/* 						<img alt="Disney Junior Live On Tour!" src="http://www.thechaifetzarena.com/images/main/DL13_PiratePrincess_thumb.jpg" />*/
/* 						<div class="info">*/
/* 							<h2 class="title">Disney Junior Live On Tour!</h2>*/
/* 							<p class="desc"> Pirate and Princess Adventure</p>*/
/* 							<ul>*/
/* 								<li style="width:33%;">$49.99 <span class="fa fa-male"></span></li>*/
/* 								<li style="width:34%;">$29.99 <span class="fa fa-child"></span></li>*/
/* 							</ul>*/
/* 						</div>*/
/* 						<div class="social">*/
/* 							<ul>*/
/* 								<li class="facebook" style="width:33%;"><a href="#facebook"><span class="fa fa-facebook"></span></a></li>*/
/* 								<li class="twitter" style="width:34%;"><a href="#twitter"><span class="fa fa-twitter"></span></a></li>*/
/* 								<li class="google-plus" style="width:33%;"><a href="#google-plus"><span class="fa fa-google-plus"></span></a></li>*/
/* 							</ul>*/
/* 						</div>*/
/* 					</li>*/
/* 				</ul>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* */
/* */
/* */
/* {% endblock %}*/
