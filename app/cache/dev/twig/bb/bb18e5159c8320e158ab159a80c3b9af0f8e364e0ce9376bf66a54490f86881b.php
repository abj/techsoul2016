<?php

/* crowdBundle:Default/acceuil:freelancer.html.twig */
class __TwigTemplate_488b636590e483ff4aee89106a17811a07805b91baea3cf7d15411e55c5bb32d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layout/layout.html.twig", "crowdBundle:Default/acceuil:freelancer.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    
               
            <!-- Title & BreadCrumbs -->             
            <section class=\"mtop\"> 
                <section class=\"container-fluid container\"> 
                    <section class=\"row-fluid\"> 
                        <section id=\"donation_box\"> 
                            <section class=\"container container-fluid\"> 
                                <section class=\"row-fluid\"> 
                                    <div class=\"span8 first\"> 
                                        <h2>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Que voulez vous faire?</h2> 
                                    </div>                                     
                                    <div class=\"span4 title_right\"> 
                                        <div class=\"dropdown\" id=\"cart_dropdown\"> 
                                            <div class=\"dropdown-menu\" aria-labelledby=\"cart_down\" role=\"menu\" id=\"listing_dropdown\"> 
                                                <ul> 
                                                    <li> Charity Events 
                                                        <a href=\"#\"> 2 </a> 
                                                    </li>                                                     
                                                    <li> Fundraising 
                                                        <a href=\"#\"> 9 </a> 
                                                    </li>                                                     
                                                    <li> Non-Profit 
                                                        <a href=\"#\"> 2 </a> 
                                                    </li>                                                     
                                                </ul>                                                 
                                            </div>                                             
                                        </div>                                         
                                    </div>                                     
                                </section>                                 
                            </section>                             
                        </section>                         
                        <!-- end of Page Title -->                         
                    </section>                     
                    <section class=\"row-fluid\"> 
                        <!-- BreadCrumbs -->                         
                        <!-- End of breadcrumbs -->                         
                    </section>                     
                </section>                 
            </section>             
            <!-- End of Tile & Breadcrumbs -->             
            <section id=\"content\" class=\"mbtm gallery\"> 
                <section class=\"container-fluid container\"> 
                    <section class=\"row-fluid\"> 
                        <ul id=\"portfolio-item-filter\" class=\"category-list\"> 
                            <li></li>                             
                            <li></li>                             
                            <li></li>                             
                            <li></li>                             
                        </ul>                         
                        <ul class=\"gallery-page\"> 
                            <li class=\"gal_img Charity span6 view_new view-tenth first\"> 
                                <figure class=\"gal_img_container\"> 
                                    <img class=\"galler-img\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img1.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                    <div class=\"mask\"> 
                                        <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                                                                <button type=\"button\" class=\"btn btn-primary \"> <h1>je veux embaucher</h1></button>

                                       
                                    </div>                                     
                                </figure>                                 
                            </li>
                            <li class=\"gal_img Events span6 view_new view-tenth\"> 
                                <figure class=\"gal_img_container\"> 
                                    <img class=\"galler-img\" src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img2.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                    <div class=\"mask\"> 
                                        <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <button type=\"button\" class=\"btn btn-primary \"> <h1>je veux travailer</h1></button>

                                       
                                    </div>                                     
                                </figure>                                 
                            </li>                             
                        </ul>                         
                    </section>                     
                </section>                 
            </section>             
            <!-- Page Content Container -->             
            <!-- Start of Footer -->             
            <footer id=\"footer\" class=\"mtp\"> 
                <!-- Start of Footer 1 -->                 
                <section class=\"footer_1\"> 
                    <section class=\"container-fluid container\"> 
                        <section class=\"row-fluid\"> 
                            <section id=\"banner\" class=\"span12 first\"> 
                                <div class=\"inner\"> 
                                    <div class=\"span9\"> 
                                        <h2> Free shipping on all orders over \$75 </h2> 
                                        <h3> * FREE OVER \$125 for international orders </h3> 
                                    </div>                                     
                                    <div class=\"pull-left span3\"> 
</div>                                     
                                </div>                                 
                            </section>                             
                            <section class=\"span12 first\" id=\"footer_main\"> 
                                <section class=\"span3 first widget\"> 
                                    <h4>GET IN <span> Touch </span> <span class=\"h-line\"></span> </h4> 
                                    <form id=\"contact_form\" method=\"post\" action=\"contact.php\"> 
                                        <input type=\"text\" value=\"Name\" name=\"name\" required /> 
                                        <input type=\"email\" value=\"Email\" name=\"email\" required /> 
                                        <textarea rows=\"5\" cols=\"20\" name=\"comments\" required>Comments</textarea>                                         
                                        <input type=\"submit\" name=\"submit\" value=\"Send Message\" /> 
                                    </form>                                     
                                </section>                                 
                                <section class=\"span3 widget popular_post\"> 
                                    <h4>Blog Popular <span> Post </span> <span class=\"h-line\"></span> </h4> 
                                    <ul id=\"popular_post\"> 
                                        <li> 
                                            <span> <i class=\"icon-facetime-video\"></i> </span> 
                                            <p> Assum mucius maiestatis et nam purto virtute\t </p>
                                        </li>                                         
                                        <li> 
                                            <span> <i class=\"icon-volume-up\"></i></span> 
                                            <p> <a href=\"#\"> Assum mucius maiestatis et nam purto virtute\t</a> </p>
                                        </li>                                         
                                        <li> 
                                            <span> <i class=\"icon-picture\"></i> </span> 
                                            <p> Assum mucius maiestatis et nam purto virtute\t </p>
                                        </li>                                         
                                        <li> 
                                            <span> <i class=\"icon-volume-up\"></i> </span> 
                                            <p> Assum mucius maiestatis et nam purto virtute\t </p>
                                        </li>                                         
                                    </ul>                                     
                                    <a class=\"v-a\" href=\"http://themeforest/user/crunchpress/\">+ View All <span class=\"h-line\"></span></a> 
                                </section>                                 
                                <section class=\"widget span3\"> 
                                    <h4>Latest Twitter <span> Updates </span> <span class=\"h-line\"></span> </h4> 
                                    <ul id=\"tweets_crunchpress\"> 
                                        <li> New Theme Called Fine Food ready to rocks! Check this out! 
                                            <span> <a href=\"#\"> http://fb.me/90DF91 </a> </span> 
                                            <span> - 2 hour ago </span> 
                                        </li>                                         
                                        <li> The Church Issues Fixed. I hope update will be ready  soon. Then you guys download it. Before upload new  
                                            <span> <a href=\"#\">http://fb.me/4rtIOlp9  </a> </span> 
                                            <span> -1 Day ago </span> 
                                        </li>                                         
                                    </ul>                                     
                                    <a class=\"v-a\" href=\"http://themeforest/user/crunchpress/\">+ View All <span class=\"h-line\"></span></a> 
                                </section>                                 
                                <section class=\"span3 widget\"> 
                                    <h4>LATEST EVENTS <span> GALLERY </span> <span class=\"h-line\"></span> </h4> 
                                    <ul class=\"gallery-list gallery_widget\"> 
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_1.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_2.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_5.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_3.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_4.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_4.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_3.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_4.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_4.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_2.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_5.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_1.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_2.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_5.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_1.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 231
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_3.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 237
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_4.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 239
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_4.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                    </ul>                                     
                                    <a class=\"v-a\" href=\"http://themeforest/user/crunchpress/\">+ View All <span class=\"h-line\"></span></a> 
                                </section>                                 
                            </section>                             
                        </section>                         
                    </section>                     
                </section>                 
               
    ";
    }

    public function getTemplateName()
    {
        return "crowdBundle:Default/acceuil:freelancer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  348 => 239,  343 => 237,  336 => 233,  331 => 231,  324 => 227,  319 => 225,  312 => 221,  307 => 219,  300 => 215,  295 => 213,  288 => 209,  283 => 207,  276 => 203,  271 => 201,  264 => 197,  259 => 195,  252 => 191,  247 => 189,  240 => 185,  235 => 183,  228 => 179,  223 => 177,  216 => 173,  211 => 171,  119 => 82,  114 => 80,  91 => 60,  86 => 58,  31 => 5,  28 => 4,  11 => 1,);
    }
}
/* {% extends "::layout/layout.html.twig" %}*/
/* */
/* */
/* {% block body %}*/
/*     */
/*                */
/*             <!-- Title & BreadCrumbs -->             */
/*             <section class="mtop"> */
/*                 <section class="container-fluid container"> */
/*                     <section class="row-fluid"> */
/*                         <section id="donation_box"> */
/*                             <section class="container container-fluid"> */
/*                                 <section class="row-fluid"> */
/*                                     <div class="span8 first"> */
/*                                         <h2>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Que voulez vous faire?</h2> */
/*                                     </div>                                     */
/*                                     <div class="span4 title_right"> */
/*                                         <div class="dropdown" id="cart_dropdown"> */
/*                                             <div class="dropdown-menu" aria-labelledby="cart_down" role="menu" id="listing_dropdown"> */
/*                                                 <ul> */
/*                                                     <li> Charity Events */
/*                                                         <a href="#"> 2 </a> */
/*                                                     </li>                                                     */
/*                                                     <li> Fundraising */
/*                                                         <a href="#"> 9 </a> */
/*                                                     </li>                                                     */
/*                                                     <li> Non-Profit */
/*                                                         <a href="#"> 2 </a> */
/*                                                     </li>                                                     */
/*                                                 </ul>                                                 */
/*                                             </div>                                             */
/*                                         </div>                                         */
/*                                     </div>                                     */
/*                                 </section>                                 */
/*                             </section>                             */
/*                         </section>                         */
/*                         <!-- end of Page Title -->                         */
/*                     </section>                     */
/*                     <section class="row-fluid"> */
/*                         <!-- BreadCrumbs -->                         */
/*                         <!-- End of breadcrumbs -->                         */
/*                     </section>                     */
/*                 </section>                 */
/*             </section>             */
/*             <!-- End of Tile & Breadcrumbs -->             */
/*             <section id="content" class="mbtm gallery"> */
/*                 <section class="container-fluid container"> */
/*                     <section class="row-fluid"> */
/*                         <ul id="portfolio-item-filter" class="category-list"> */
/*                             <li></li>                             */
/*                             <li></li>                             */
/*                             <li></li>                             */
/*                             <li></li>                             */
/*                         </ul>                         */
/*                         <ul class="gallery-page"> */
/*                             <li class="gal_img Charity span6 view_new view-tenth first"> */
/*                                 <figure class="gal_img_container"> */
/*                                     <img class="galler-img" src="{{asset('images/gallery_img1.jpg')}}" alt=""> */
/*                                     <div class="mask"> */
/*                                         <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_3.jpg')}}" title="">&nbsp;</a> */
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                                                                 <button type="button" class="btn btn-primary "> <h1>je veux embaucher</h1></button>*/
/* */
/*                                        */
/*                                     </div>                                     */
/*                                 </figure>                                 */
/*                             </li>*/
/*                             <li class="gal_img Events span6 view_new view-tenth"> */
/*                                 <figure class="gal_img_container"> */
/*                                     <img class="galler-img" src="{{asset('images/gallery_img2.jpg')}}" alt=""> */
/*                                     <div class="mask"> */
/*                                         <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_2.jpg')}}" title="">&nbsp;</a> */
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <br>*/
/*                                         <button type="button" class="btn btn-primary "> <h1>je veux travailer</h1></button>*/
/* */
/*                                        */
/*                                     </div>                                     */
/*                                 </figure>                                 */
/*                             </li>                             */
/*                         </ul>                         */
/*                     </section>                     */
/*                 </section>                 */
/*             </section>             */
/*             <!-- Page Content Container -->             */
/*             <!-- Start of Footer -->             */
/*             <footer id="footer" class="mtp"> */
/*                 <!-- Start of Footer 1 -->                 */
/*                 <section class="footer_1"> */
/*                     <section class="container-fluid container"> */
/*                         <section class="row-fluid"> */
/*                             <section id="banner" class="span12 first"> */
/*                                 <div class="inner"> */
/*                                     <div class="span9"> */
/*                                         <h2> Free shipping on all orders over $75 </h2> */
/*                                         <h3> * FREE OVER $125 for international orders </h3> */
/*                                     </div>                                     */
/*                                     <div class="pull-left span3"> */
/* </div>                                     */
/*                                 </div>                                 */
/*                             </section>                             */
/*                             <section class="span12 first" id="footer_main"> */
/*                                 <section class="span3 first widget"> */
/*                                     <h4>GET IN <span> Touch </span> <span class="h-line"></span> </h4> */
/*                                     <form id="contact_form" method="post" action="contact.php"> */
/*                                         <input type="text" value="Name" name="name" required /> */
/*                                         <input type="email" value="Email" name="email" required /> */
/*                                         <textarea rows="5" cols="20" name="comments" required>Comments</textarea>                                         */
/*                                         <input type="submit" name="submit" value="Send Message" /> */
/*                                     </form>                                     */
/*                                 </section>                                 */
/*                                 <section class="span3 widget popular_post"> */
/*                                     <h4>Blog Popular <span> Post </span> <span class="h-line"></span> </h4> */
/*                                     <ul id="popular_post"> */
/*                                         <li> */
/*                                             <span> <i class="icon-facetime-video"></i> </span> */
/*                                             <p> Assum mucius maiestatis et nam purto virtute	 </p>*/
/*                                         </li>                                         */
/*                                         <li> */
/*                                             <span> <i class="icon-volume-up"></i></span> */
/*                                             <p> <a href="#"> Assum mucius maiestatis et nam purto virtute	</a> </p>*/
/*                                         </li>                                         */
/*                                         <li> */
/*                                             <span> <i class="icon-picture"></i> </span> */
/*                                             <p> Assum mucius maiestatis et nam purto virtute	 </p>*/
/*                                         </li>                                         */
/*                                         <li> */
/*                                             <span> <i class="icon-volume-up"></i> </span> */
/*                                             <p> Assum mucius maiestatis et nam purto virtute	 </p>*/
/*                                         </li>                                         */
/*                                     </ul>                                     */
/*                                     <a class="v-a" href="http://themeforest/user/crunchpress/">+ View All <span class="h-line"></span></a> */
/*                                 </section>                                 */
/*                                 <section class="widget span3"> */
/*                                     <h4>Latest Twitter <span> Updates </span> <span class="h-line"></span> </h4> */
/*                                     <ul id="tweets_crunchpress"> */
/*                                         <li> New Theme Called Fine Food ready to rocks! Check this out! */
/*                                             <span> <a href="#"> http://fb.me/90DF91 </a> </span> */
/*                                             <span> - 2 hour ago </span> */
/*                                         </li>                                         */
/*                                         <li> The Church Issues Fixed. I hope update will be ready  soon. Then you guys download it. Before upload new  */
/*                                             <span> <a href="#">http://fb.me/4rtIOlp9  </a> </span> */
/*                                             <span> -1 Day ago </span> */
/*                                         </li>                                         */
/*                                     </ul>                                     */
/*                                     <a class="v-a" href="http://themeforest/user/crunchpress/">+ View All <span class="h-line"></span></a> */
/*                                 </section>                                 */
/*                                 <section class="span3 widget"> */
/*                                     <h4>LATEST EVENTS <span> GALLERY </span> <span class="h-line"></span> </h4> */
/*                                     <ul class="gallery-list gallery_widget"> */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_1.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_3.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_2.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_5.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_3.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_2.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_4.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_4.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_3.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_2.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_4.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_4.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_2.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_5.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_1.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_3.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_2.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_5.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_1.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_3.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_3.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_2.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_4.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_4.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                     </ul>                                     */
/*                                     <a class="v-a" href="http://themeforest/user/crunchpress/">+ View All <span class="h-line"></span></a> */
/*                                 </section>                                 */
/*                             </section>                             */
/*                         </section>                         */
/*                     </section>                     */
/*                 </section>                 */
/*                */
/*     {% endblock %}*/
