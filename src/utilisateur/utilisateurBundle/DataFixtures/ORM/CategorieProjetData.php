<?php
namespace utilisateur\utilisateurBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use utilisateur\utilisateurBundle\Entity\CategorieProjet;

class CategorieProjetData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $CategorieProjet = new CategorieProjet();
        $CategorieProjet->setCategorie('Web Design');
        $manager->persist($CategorieProjet);
        
        
        
        $CategorieProjet2 = new CategorieProjet();
        $CategorieProjet2->setCategorie('Web Developpement'); 
        $manager->persist($CategorieProjet2);
       
       
        $CategorieProjet3 = new CategorieProjet();
        $CategorieProjet3->setCategorie('Sport'); 
        $manager->persist($CategorieProjet3);
       
       
        $CategorieProjet4 = new CategorieProjet();
        $CategorieProjet4->setCategorie('Nature et envirenement'); 
        $manager->persist($CategorieProjet4);
       
       
       
       
       
        $CategorieProjet5 = new CategorieProjet();
        $CategorieProjet5->setCategorie('reparation  informatique'); 
        $manager->persist($CategorieProjet5);
       
       
       
        $CategorieProjet6 = new CategorieProjet();
        $CategorieProjet6->setCategorie('Mobile Developement'); 
        $manager->persist($CategorieProjet6);

        
        
        
        
        
        $CategorieProjet7 = new CategorieProjet();
        $CategorieProjet7->setCategorie('Bricolage'); 
        $manager->persist($CategorieProjet7);
       
       
        
        
        
        $CategorieProjet8 = new CategorieProjet();
        $CategorieProjet8->setCategorie('nouvelle technologie'); 
        $manager->persist($CategorieProjet8);

        
        
        
       
        $CategorieProjet9 = new CategorieProjet();
        $CategorieProjet9->setCategorie('Divertissement'); 
        $manager->persist($CategorieProjet9);
       
       
       
        $CategorieProjet10 = new CategorieProjet();
        $CategorieProjet10->setCategorie('humanitaire'); 
        $manager->persist($CategorieProjet10);
       
       
       
       
       
       
       
       
       
       
       
       
        $manager->flush();
        
        
   
         $this->addReference('CategorieProjet', $CategorieProjet);
         $this->addReference('CategorieProjet2', $CategorieProjet2);
         $this->addReference('CategorieProjet3', $CategorieProjet2);
         $this->addReference('CategorieProjet4', $CategorieProjet2);
         $this->addReference('CategorieProjet5', $CategorieProjet2);
         $this->addReference('CategorieProjet6', $CategorieProjet2);
         $this->addReference('CategorieProjet7', $CategorieProjet2);
         $this->addReference('CategorieProjet8', $CategorieProjet2);
         $this->addReference('CategorieProjet9', $CategorieProjet2);
         $this->addReference('CategorieProjet10', $CategorieProjet2);
         
        }
         public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
    
}