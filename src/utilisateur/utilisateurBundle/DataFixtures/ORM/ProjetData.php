<?php

namespace utilisateur\utilisateurBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use utilisateur\utilisateurBundle\Entity\Projet;

class ProjetData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        
        
        
        for ($i = 1; $i <= 10; $i++)
        {
            
        $Projet = new Projet();
        $Projet->setNomProjet('Banc de riche');
        $Projet->setImageFile('tumblr_nxfxo62u2F1r20q0do4_540.jpg');
        $Projet->setBody("hello word");
//        $Projet->setDepth('2');
        $Projet->setState('2');
        $Projet->setAncestors( array('A', 'B', 'C', 'D'));
        $Projet->setCreatedAt(new \DateTime('1970-1-1'));
        $Projet->setBudjet('25,000');
        $Projet->setargent('25,000');
        $Projet->setResume('Mauris auctor blandit neque eu cursus. Nunc vel commodo dui, sed tempus mi. Fusce eleifend, orci ut elementum porta, mauris leo porta purus.

Etiam aliquet tempor est nec pharetra. Etiam interdum tincidunt orci vitae elementum. Donec sollicitudin quis risus sit amet lobortis. Fusce sed tincidunt nisl.');
        $Projet->setImage('http://www.gettyimages.ca/gi-resources/images/Homepage/Category-Creative/UK/UK_Creative_462809583.jpg');
        $Projet->setFichier('WWWWWWWWW');
        $Projet->setIdCategorieProjet($this->getReference('CategorieProjet'));
        $Projet->setIdType($this->getReference('TypeProjet'));
        //  $Projet->setId($this->getReference('User3'));

        
        $manager->persist($Projet);


        }







        $manager->flush();
    }

    public function getOrder() {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 3;
    }

}
